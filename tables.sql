-- phpMyAdmin SQL Dump
-- version 2.9.0.2
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Jan 19, 2007 at 03:56 PM
-- Server version: 4.1.21
-- PHP Version: 4.4.2
-- 
-- Database: `core_sms`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `core_carriers`
-- 

CREATE TABLE `core_carriers` (
  `id` int(11) NOT NULL auto_increment,
  `carrier` varchar(50) NOT NULL default '',
  `domain` varchar(30) NOT NULL default '',
  `country` varchar(15) NOT NULL default '',
  `reliable` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=143 ;

-- 
-- Dumping data for table `core_carriers`
-- 

INSERT INTO `core_carriers` VALUES (1, '', '', '', 0);
INSERT INTO `core_carriers` VALUES (2, 'Unknown', 'Unknown', '', 0);
INSERT INTO `core_carriers` VALUES (3, '3River Wireless', 'sms.3rivers.net', '', 0);
INSERT INTO `core_carriers` VALUES (4, 'Andhra Pradesh Airtel', 'airtelap.com', '', 0);
INSERT INTO `core_carriers` VALUES (5, 'Andhra Pradesh Idea Cellular', 'ideacellular.net', '', 0);
INSERT INTO `core_carriers` VALUES (6, 'Alltel PCS', 'message.alltel.com', '', 0);
INSERT INTO `core_carriers` VALUES (7, 'Alltel', 'alltelmessage.com', '', 0);
INSERT INTO `core_carriers` VALUES (8, 'Arch Wireless', 'archwireless.net', '', 0);
INSERT INTO `core_carriers` VALUES (9, 'BeeLine GSM', 'sms.beemail.ru', '', 0);
INSERT INTO `core_carriers` VALUES (10, 'BeeLine (Moscow)', 'sms.gate.ru', '', 0);
INSERT INTO `core_carriers` VALUES (11, 'Bell Canada', 'txt.bellmobility.ca', '', 0);
INSERT INTO `core_carriers` VALUES (12, 'Bell Canada', 'bellmobility.ca', '', 0);
INSERT INTO `core_carriers` VALUES (13, 'Bell Atlantic', 'message.bam.com', '', 0);
INSERT INTO `core_carriers` VALUES (14, 'Bell South', 'sms.bellsouth.com', '', 0);
INSERT INTO `core_carriers` VALUES (15, 'Bell South', 'wireless.bellsouth.com', '', 0);
INSERT INTO `core_carriers` VALUES (16, 'Bite GSM (Lithuania)', 'sms.bite.lt', '', 0);
INSERT INTO `core_carriers` VALUES (17, 'Bluegrass Cellular', 'sms.bluecell.com', '', 0);
INSERT INTO `core_carriers` VALUES (18, 'BPL mobile', 'bplmobile.com', '', 0);
INSERT INTO `core_carriers` VALUES (19, 'Celcom (Malaysia)', 'sms.celcom.com.my', '', 0);
INSERT INTO `core_carriers` VALUES (20, 'Cellular One', 'mobile.celloneusa.com', 'US', 10);
INSERT INTO `core_carriers` VALUES (21, 'Cellular One East Cost', 'phone.cellone.net', '', 0);
INSERT INTO `core_carriers` VALUES (22, 'Cellular South', 'csouth1.com', '', 0);
INSERT INTO `core_carriers` VALUES (23, 'Cellular One', 'sbcemail.com', '', 0);
INSERT INTO `core_carriers` VALUES (24, 'Cingular', 'mobile.mycingular.net', 'US', 10);
INSERT INTO `core_carriers` VALUES (26, 'CZECH EuroTel', 'sms.eurotel.cz', '', 0);
INSERT INTO `core_carriers` VALUES (27, 'Chennai Skycell / Airtel', 'airtelchennai.com', '', 0);
INSERT INTO `core_carriers` VALUES (28, 'Comviq GSM Sweden', 'sms.comviq.se', '', 0);
INSERT INTO `core_carriers` VALUES (29, 'Corr Wireless Communications', 'corrwireless.net', '', 0);
INSERT INTO `core_carriers` VALUES (30, 'D1 De TeMobil', 't-d1-sms.de', '', 0);
INSERT INTO `core_carriers` VALUES (31, 'D2 Mannesmann Mobilefunk', 'd2-message.de', '', 0);
INSERT INTO `core_carriers` VALUES (32, 'DT T-Mobile', 't-mobile-sms.de', '', 0);
INSERT INTO `core_carriers` VALUES (33, 'Delhi Airtel', 'airtelmail.com', '', 0);
INSERT INTO `core_carriers` VALUES (34, 'Delhi Hutch', 'delhi.hutch.co.in', '', 0);
INSERT INTO `core_carriers` VALUES (35, 'Dobson Cellular Systems', 'mobile.dobson.net', '', 0);
INSERT INTO `core_carriers` VALUES (36, 'E-Plus (Germany)', 'eplus.de', '', 0);
INSERT INTO `core_carriers` VALUES (37, 'EMT', 'sms.emt.ee', '', 0);
INSERT INTO `core_carriers` VALUES (38, 'Eurotel (Czech Republic)', 'sms.eurotel.cz', '', 0);
INSERT INTO `core_carriers` VALUES (39, 'Escotel', 'escotelmobile.com', '', 0);
INSERT INTO `core_carriers` VALUES (40, 'Estonia EMT', 'sms-m.emt.ee', '', 0);
INSERT INTO `core_carriers` VALUES (41, 'Estonia RLE', 'rle.ee', '', 0);
INSERT INTO `core_carriers` VALUES (42, 'Estonia Q GSM', 'qgsm.ee', '', 0);
INSERT INTO `core_carriers` VALUES (43, 'Estonia Mobil Telephone', 'sms.emt.ee', '', 0);
INSERT INTO `core_carriers` VALUES (44, 'Fido', 'fido.ca', '', 0);
INSERT INTO `core_carriers` VALUES (45, 'Georgea geocell', 'sms.ge', '', 0);
INSERT INTO `core_carriers` VALUES (46, 'Goa BPLMobil', 'bplmobile.com', '', 0);
INSERT INTO `core_carriers` VALUES (47, 'Golden Telecom', 'sms.goldentele.com', '', 0);
INSERT INTO `core_carriers` VALUES (48, 'Golden Telecom (Kiev, Ukraine only)', 'sms.gt.kiev.ua', '', 0);
INSERT INTO `core_carriers` VALUES (49, 'GTE', 'messagealert.com', '', 0);
INSERT INTO `core_carriers` VALUES (50, 'GTE', 'airmessage.net', '', 0);
INSERT INTO `core_carriers` VALUES (51, 'Gujarat Idea', 'ideacellular.net', '', 0);
INSERT INTO `core_carriers` VALUES (52, 'Gujarat Airtel', 'airtelmail.com', '', 0);
INSERT INTO `core_carriers` VALUES (53, 'Goa Airtel', 'airtelmail.com', '', 0);
INSERT INTO `core_carriers` VALUES (54, 'Goa BPLMobil', 'bplmobile.com', '', 0);
INSERT INTO `core_carriers` VALUES (55, 'Goa Idea Cellular', 'ideacellular.net', '', 0);
INSERT INTO `core_carriers` VALUES (56, 'Haryana Airtel', 'airtelmail.com', '', 0);
INSERT INTO `core_carriers` VALUES (57, 'Haryana Escotel', 'escotelmobile.com', '', 0);
INSERT INTO `core_carriers` VALUES (58, 'Himachal Pradesh Airtel', 'airtelmail.com', '', 0);
INSERT INTO `core_carriers` VALUES (59, 'Hungary Pannon GSM', 'sms.pgsm.hu', '', 0);
INSERT INTO `core_carriers` VALUES (60, 'Idea Cellular', 'ideacellular.net', '', 0);
INSERT INTO `core_carriers` VALUES (61, 'Inland Cellular Telephone', 'inlandlink.com', '', 0);
INSERT INTO `core_carriers` VALUES (62, 'Israel Orange IL', 'shiny.co.il', '', 0);
INSERT INTO `core_carriers` VALUES (63, 'Karnataka Airtel', 'airtelkk.com', '', 0);
INSERT INTO `core_carriers` VALUES (64, 'Kerala Airtel', 'airtelmail.com', '', 0);
INSERT INTO `core_carriers` VALUES (65, 'Kerala Escotel', 'escotelmobile.com', '', 0);
INSERT INTO `core_carriers` VALUES (66, 'Kerala BPL Mobile', 'bplmobile.com', '', 0);
INSERT INTO `core_carriers` VALUES (67, 'Kyivstar  (Kiev Ukraine only)', 'sms.kyivstar.net', '', 0);
INSERT INTO `core_carriers` VALUES (68, 'Latvia TELE2', 'sms.tele2.lv', '', 0);
INSERT INTO `core_carriers` VALUES (69, 'Madhya Pradesh Airtel', 'airtelmail.com', '', 0);
INSERT INTO `core_carriers` VALUES (70, 'Maharashtra Idea Cellular', 'ideacellular.net', '', 0);
INSERT INTO `core_carriers` VALUES (71, 'MCI Phone', 'mci.com', '', 0);
INSERT INTO `core_carriers` VALUES (72, 'Meteor', 'mymeteor.ie', '', 0);
INSERT INTO `core_carriers` VALUES (73, 'Metro PCS', 'mymetropcs.com', '', 0);
INSERT INTO `core_carriers` VALUES (74, 'MiWorld', 'm1.com.sg', '', 0);
INSERT INTO `core_carriers` VALUES (75, 'Mobileone', 'm1.com.sg', '', 0);
INSERT INTO `core_carriers` VALUES (76, 'Mobilecomm', 'mobilecomm.net', '', 0);
INSERT INTO `core_carriers` VALUES (77, 'Mobtel  Srbija', 'mobtel.co.yu', '', 0);
INSERT INTO `core_carriers` VALUES (78, 'Mobistar Belgium', 'mobistar.be', '', 0);
INSERT INTO `core_carriers` VALUES (79, 'Mobility Bermuda', 'ml.bm', '', 0);
INSERT INTO `core_carriers` VALUES (80, 'Maharashtra Airtel', 'airtelmail.com', '', 0);
INSERT INTO `core_carriers` VALUES (81, 'Maharashtra BPL Mobile', 'bplmobile.com', '', 0);
INSERT INTO `core_carriers` VALUES (82, 'Manitoba Telecom Systems', 'text.mtsmobility.com', '', 0);
INSERT INTO `core_carriers` VALUES (83, 'Mumbai BPL Mobile', 'bplmobile.com', '', 0);
INSERT INTO `core_carriers` VALUES (84, 'MTN (South Africa only )', 'sms.co.za', '', 0);
INSERT INTO `core_carriers` VALUES (85, 'MiWorld ( Singapore)', 'm1.com.sg', '', 0);
INSERT INTO `core_carriers` VALUES (86, 'NBTel', 'wirefree.informe.ca', '', 0);
INSERT INTO `core_carriers` VALUES (87, 'Netcom GSM (Norway)', 'sms.netcom.no', '', 0);
INSERT INTO `core_carriers` VALUES (88, 'Nextel', 'messaging.nextel.com', '', 0);
INSERT INTO `core_carriers` VALUES (89, 'Nextel', 'nextel.com.br', '', 0);
INSERT INTO `core_carriers` VALUES (90, 'Optimus (Portugal)', 'sms.optimus.pt', '', 0);
INSERT INTO `core_carriers` VALUES (91, 'Orange', 'orange.net', '', 0);
INSERT INTO `core_carriers` VALUES (92, 'Oskar', 'mujoskar.cz', '', 0);
INSERT INTO `core_carriers` VALUES (93, 'Pacific Bell', 'pacbellpcs.net', '', 0);
INSERT INTO `core_carriers` VALUES (94, 'PlusGSM (Poland only)', 'text.plusgsm.pl', '', 0);
INSERT INTO `core_carriers` VALUES (95, 'P&T Luxembourg', 'sms.luxgsm.lu', '', 0);
INSERT INTO `core_carriers` VALUES (96, 'Poland PLUS GSM', 'text.plusgsm.pl', '', 0);
INSERT INTO `core_carriers` VALUES (97, 'Pondicherry BPL Mobile', 'bplmobile.com', '', 0);
INSERT INTO `core_carriers` VALUES (98, 'Primtel', 'sms.primtel.ru', '', 0);
INSERT INTO `core_carriers` VALUES (99, 'Punjab Airtel', 'airtelmail.com', '', 0);
INSERT INTO `core_carriers` VALUES (100, 'Qwest', 'qwestmp.com', '', 0);
INSERT INTO `core_carriers` VALUES (101, 'Rogers AT&T Wireless', 'pcs.rogers.com', '', 0);
INSERT INTO `core_carriers` VALUES (102, 'Safaricom', 'safaricomsms.com', '', 0);
INSERT INTO `core_carriers` VALUES (103, 'Satelindo GSM', 'satelindogsm.com', '', 0);
INSERT INTO `core_carriers` VALUES (104, 'Simobile (Slovenia)', 'simobil.net', '', 0);
INSERT INTO `core_carriers` VALUES (105, 'SCS-900', 'scs-900.ru', '', 0);
INSERT INTO `core_carriers` VALUES (106, 'Sunrise Mobile', 'mysunrise.ch', '', 0);
INSERT INTO `core_carriers` VALUES (107, 'Sunrise Mobile', 'freesurf.ch', '', 0);
INSERT INTO `core_carriers` VALUES (108, 'SFR France', 'sfr.fr', '', 0);
INSERT INTO `core_carriers` VALUES (109, 'Southwestern Bell', 'email.swbw.com', '', 0);
INSERT INTO `core_carriers` VALUES (110, 'Sprint PCS', 'messaging.sprintpcs.com', '', 0);
INSERT INTO `core_carriers` VALUES (111, 'Sprint', 'sprintpaging.com', '', 0);
INSERT INTO `core_carriers` VALUES (112, 'Swisscom', 'bluewin.ch', '', 0);
INSERT INTO `core_carriers` VALUES (113, 'Swisscom', 'bluemail.ch', '', 0);
INSERT INTO `core_carriers` VALUES (114, 'Telecom Italia Mobile (Italy)', 'posta.tim.it', '', 0);
INSERT INTO `core_carriers` VALUES (115, 'Telenor Mobil  Norway', 'mobilpost.com', '', 0);
INSERT INTO `core_carriers` VALUES (116, 'Telecel (Portugal)', 'sms.telecel.pt', '', 0);
INSERT INTO `core_carriers` VALUES (117, 'Tele2', 'sms.tele2.lv', '', 0);
INSERT INTO `core_carriers` VALUES (118, 'Telus', 'msg.telus.com', '', 0);
INSERT INTO `core_carriers` VALUES (119, 'Telenor', 'mobilpost.no', '', 0);
INSERT INTO `core_carriers` VALUES (120, 'Telia Denmark', 'gsm1800.telia.dk', '', 0);
INSERT INTO `core_carriers` VALUES (121, 'TMN (Portugal)', 'mail.tmn.pt', '', 0);
INSERT INTO `core_carriers` VALUES (122, 'T-Mobile Austria', 'sms.t-mobile.at', '', 0);
INSERT INTO `core_carriers` VALUES (123, 'T-Mobile Germany', 't-d1-sms.de', '', 0);
INSERT INTO `core_carriers` VALUES (124, 'T-Mobile UK', 't-mobile.uk.net', '', 0);
INSERT INTO `core_carriers` VALUES (125, 'T-Mobile USA', 'tmomail.net', '', 0);
INSERT INTO `core_carriers` VALUES (126, 'Triton', 'tms.suncom.com', '', 0);
INSERT INTO `core_carriers` VALUES (127, 'Tamil Nadu BPL Mobile', 'bplmobile.com', '', 0);
INSERT INTO `core_carriers` VALUES (128, 'UMC GSM', 'sms.umc.com.ua', '', 0);
INSERT INTO `core_carriers` VALUES (129, 'Unicel', 'utext.com', '', 0);
INSERT INTO `core_carriers` VALUES (130, 'Uraltel', 'sms.uraltel.ru', '', 0);
INSERT INTO `core_carriers` VALUES (131, 'US Cellular', 'email.uscc.net', '', 0);
INSERT INTO `core_carriers` VALUES (132, 'Uttar Pradesh (West) Escotel', 'escotelmobile.com', '', 0);
INSERT INTO `core_carriers` VALUES (133, 'Verizon', 'vtext.com', '', 0);
INSERT INTO `core_carriers` VALUES (134, 'Vodafone Omnitel (Italy)', 'vizzavi.it', '', 0);
INSERT INTO `core_carriers` VALUES (135, 'Vodafone Italy', 'sms.vodafone.it', '', 0);
INSERT INTO `core_carriers` VALUES (136, 'Vodafone Japan', 'c.vodafone.ne.jp', '', 0);
INSERT INTO `core_carriers` VALUES (137, 'Vodafone Japan', 'h.vodafone.ne.jp', '', 0);
INSERT INTO `core_carriers` VALUES (138, 'Vodafone Japan', 't.vodafone.ne.jp', '', 0);
INSERT INTO `core_carriers` VALUES (139, 'Vodafone Spain', 'vodafone.es', '', 0);
INSERT INTO `core_carriers` VALUES (140, 'Vodafone UK', 'vodafone.net', '', 0);
INSERT INTO `core_carriers` VALUES (141, 'West Central Wireless', 'sms.wcc.net', '', 0);
INSERT INTO `core_carriers` VALUES (142, 'Western Wireless', 'cellularonewest.com', '', 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `core_logs`
-- 

CREATE TABLE `core_logs` (
  `id` int(11) NOT NULL auto_increment,
  `domain` varchar(30) NOT NULL default '',
  `number` varchar(10) NOT NULL default '',
  `sender_ip` varchar(15) NOT NULL default '',
  `date_time` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- 
-- Dumping data for table `core_logs`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `core_users`
-- 

CREATE TABLE `core_users` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(30) NOT NULL default '',
  `username` varchar(20) NOT NULL default '',
  `password` varchar(50) NOT NULL default '',
  `carrier` varchar(5) NOT NULL default '',
  `number` varchar(10) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `core_users`
-- 

